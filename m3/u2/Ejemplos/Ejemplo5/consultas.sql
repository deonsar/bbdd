﻿/* Consultas de Combinación Externa - 5 */

/* 1 - Nombre y edad de los ciclistas que NO han ganado etapas */
SELECT DISTINCT nombre,edad FROM ciclista c LEFT JOIN etapa e ON c.dorsal=e.dorsal WHERE e.dorsal IS NULL;

/* 2 - Nombre y edad de los ciclistas que NO han ganado puertos */
SELECT DISTINCT nombre,edad FROM ciclista c LEFT JOIN puerto p ON c.dorsal=p.dorsal WHERE p.dorsal IS NULL;

/* 3 - Listar el director de los equipos que tengan ciclistas NO que hayan ganado NINGUNA etapa */
SELECT DISTINCT director 
FROM equipo e 
JOIN ciclista c ON e.nomequipo=c.nomequipo
LEFT JOIN etapa et ON c.dorsal = et.dorsal WHERE et.dorsal IS null;

-- C1
SELECT DISTINCT c.nomequipo FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal WHERE e.dorsal IS NULL; 

-- equipo JOIN C1
SELECT 
  director 
  FROM 
    equipo e 
  JOIN (
    SELECT DISTINCT 
      c.nomequipo 
    FROM 
      ciclista c 
    LEFT JOIN 
      etapa e 
    ON 
      c.dorsal = e.dorsal 
    WHERE 
      e.dorsal IS NULL
  ) c1
ON e.nomequipo=c1.nomequipo;

/* 4 - Dorsal y nombre de los ciclistas que NO hayan llevado algun maillot */
SELECT c.dorsal,c.nombre FROM ciclista c LEFT JOIN lleva l ON c.dorsal = l.dorsal WHERE l.dorsal IS NULL;


/* 5 - Dorsal y nombre de los ciclistas que NO hayan llevado maillot amarillo NUNCA */
-- c1 - Ciclistas llevan maillot
SELECT DISTINCT l.dorsal FROM lleva l;  

-- c2 - Ciclistas que llevan el maillot amarillo
SELECT DISTINCT l.dorsal FROM lleva l INNER JOIN maillot m ON l.código=m.código WHERE m.color='Amarillo'; 

-- c3 = (C1 - C2) - Ciclistas que no han llevado el unicamente maillot amarillo
SELECT 
  c1.dorsal
  FROM 
    (SELECT DISTINCT l.dorsal FROM lleva l) c1 
  LEFT JOIN (
    SELECT DISTINCT l.dorsal FROM lleva l INNER JOIN maillot m ON l.código=m.código WHERE m.color='Amarillo'
  ) c2 
  ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL;

-- ciclistas JOIN C3
SELECT c.dorsal,c.nombre 
    FROM ciclista c 
    JOIN (
      SELECT 
        c1.dorsal
      FROM 
        (SELECT DISTINCT l.dorsal FROM lleva l) c1 
      LEFT JOIN (
        SELECT DISTINCT l.dorsal FROM lleva l INNER JOIN maillot m ON l.código=m.código WHERE m.color='Amarillo'
      ) c2 
      ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL) c3 
    ON c.dorsal=c3.dorsal;
  
/* 6 - Indicar el numetapa de las etapas que NO tengan puertos*/
SELECT e.numetapa FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL;

/* 7 - Indicar la distancia media de las etapas que NO tengan puertos  */
SELECT AVG(e.kms) FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL;

/* 8 - Listar el número de ciclistas que NO hayan ganado alguna etapa */
-- C1
SELECT c.dorsal FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal WHERE e.dorsal IS NULL;

SELECT COUNT(*) FROM (SELECT c.dorsal FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal WHERE e.dorsal IS NULL) c1;

/* 9 - Listar el dorsal de los ciclistas que hayan ganado alguna etapa que NO tenga puerto */

SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL;

/* 10 - Listar el dorsal de los ciclistas que hayan ganado unicamente etapas que no tenga puertos */
SELECT c1.dorsal 
  FROM   
     (SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL) c1
  LEFT JOIN
     (SELECT DISTINCT e.dorsal FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa)  c2
  ON c1.dorsal=c2.dorsal
  WHERE c2.dorsal IS null;
    
SELECT DISTINCT e.dorsal FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa;

SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL ;  


