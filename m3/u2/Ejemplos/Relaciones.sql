﻿-- NATURAL JOIN - Automaticamente se iguala con los campos del mismo nombre
SELECT * FROM ciclista NATURAL JOIN etapa; 

-- INNER JOIN con "ON"
SELECT * FROM  ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal;

-- Producto cartesiano con "WHERE"
SELECT * FROM  ciclista,etapa WHERE ciclista.dorsal=etapa.dorsal;

-- INNER JOIN con "Using"
SELECT * FROM ciclista INNER JOIN etapa USING(dorsal);


/* PD: JOIN por defecto equivale a INNER JOIN  */

SELECT * FROM ciclista JOIN puerto;
SELECT * FROM ciclista INNER JOIN puerto ON ciclista.dorsal=puerto.dorsal;
SELECT * FROM ciclista,etapa WHERE ciclista.dorsal=puerto.dorsal;
SELECT * FROM ciclista INNER JOIN puerto USING(dorsal);


/* INNER JOIN CON TRES TABLAS */
-- opcion 1
SELECT * FROM ciclista c INNER JOIN equipo INNER JOIN etapa ON c.nomequipo = equipo.nomequipo AND c.dorsal = etapa.dorsal;

-- opcion 2
SELECT * 
FROM 
  equipo
  INNER JOIN ciclista c ON c.nomequipo = equipo.nomequipo 
  INNER JOIN etapa ON c.dorsal = etapa.dorsal;

-- opcion 3
SELECT * 
FROM  
  equipo 
  JOIN ciclista USING(nomequipo)
  JOIN etapa USING(dorsal)



  