﻿/* Consultas de Combinación Interna - 4 */

/* 1 - Nombre y edad de los ciclistas que han ganado etapas */
select DISTINCT nombre,edad from ciclista c INNER JOIN etapa e ON c.dorsal=e.dorsal;

/* 2 - Nombre y edad de los ciclistas que han ganado puertos */
select DISTINCT nombre,edad from ciclista c INNER JOIN puerto p ON c.dorsal=p.dorsal;
 
/* 3 - Nombre y edad de los ciclistas que han ganado etapas y puertos */
SELECT DISTINCT nombre,edad FROM ciclista c 
  INNER JOIN etapa e ON c.dorsal=e.dorsal
  INNER JOIN puerto p ON c.dorsal=p.dorsal;

/* 4 - Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa */
SELECT DISTINCT director FROM equipo eq 
  INNER JOIN ciclista c ON eq.nomequipo=c.nomequipo
  INNER JOIN etapa et ON c.dorsal=et.dorsal;

/* 5 - Dorsal y nombre de los ciclistas que hayan llevado algún maillot */
SELECT DISTINCT c.dorsal,nombre FROM ciclista c INNER JOIN lleva l ON  c.dorsal=l.dorsal;

/* 6 - Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo */
SELECT DISTINCT 
    c.dorsal,nombre 
  FROM 
    ciclista c 
  INNER JOIN 
    lleva l ON  c.dorsal=l.dorsal 
  INNER JOIN
    maillot m ON l.código=m.código
  WHERE 
    color='Amarillo';

/* 7 - Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas */
SELECT DISTINCT l.dorsal FROM lleva l INNER JOIN etapa e ON l.numetapa=e.numetapa;

/* 8 - Indicar el numetapa de las etapas que tengan puertos  */
SELECT DISTINCT e.numetapa FROM etapa e JOIN puerto p ON e.numetapa=p.numetapa;

/* 9 - Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos */
SELECT DISTINCT kms FROM etapa e 
  INNER JOIN ciclista c ON e.dorsal=c.dorsal 
  INNER JOIN puerto p ON e.numetapa=p.numetapa
  WHERE c.nomequipo='Banesto';

/* 10 - Listar el número de ciclistas que hayan ganado alguna etapa con puerto */

SELECT  DISTINCT c.dorsal FROM ciclista c 
  INNER JOIN etapa e ON c.dorsal=e.dorsal
  INNER JOIN puerto p ON p.numetapa=e.numetapa; 

SELECT COUNT(*) FROM (SELECT  DISTINCT c.dorsal FROM ciclista c 
  INNER JOIN etapa e ON c.dorsal=e.dorsal
  INNER JOIN puerto p ON p.numetapa=e.numetapa) C1;

/* 11 - Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto */
SELECT DISTINCT nompuerto FROM puerto p 
  INNER JOIN ciclista c ON p.dorsal=c.dorsal
  WHERE nomequipo='Banesto';

/* 12 - Listar el número de etapas que tengan puerto que hayan sido ganado por ciclistas de Banesto con mas 200 km4 */

SELECT DISTINCT e.numetapa 
  FROM etapa e 
    INNER JOIN puerto p ON e.numetapa=p.numetapa 
    INNER JOIN ciclista c ON e.dorsal=c.dorsal 
    WHERE nomequipo='Banesto' AND e.kms>200;

SELECT COUNT(*) FROM 
    (SELECT DISTINCT e.numetapa FROM etapa e 
    INNER JOIN puerto p ON e.numetapa=p.numetapa 
    INNER JOIN ciclista c ON e.dorsal=c.dorsal 
    WHERE nomequipo='Banesto' AND e.kms>200) c1;