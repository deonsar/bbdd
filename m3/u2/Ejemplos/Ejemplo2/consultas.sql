﻿/* Consultas de Totales - 2 */

/* 1 - Número de ciclistas que hay */
SELECT COUNT(*)total FROM ciclista;

/* 2 - Número de ciclistas que hay del equipo Banesto*/
SELECT COUNT(*)total FROM ciclista WHERE nomequipo='banesto';
select count(*) from ciclista where nomequipo like 'Banesto';
/* 3 - La edad media de los ciclistas */
SELECT AVG(edad) edadMedia FROM ciclista;

/* 4 - La edad media de los ciclistas del equipo Banesto*/
SELECT AVG(edad)media FROM ciclista WHERE nomequipo='banesto';

/* 5 - La edad media de los ciclistas por cada equipo */
SELECT nomequipo,AVG(edad)media FROM ciclista GROUP BY nomequipo; 

/* 6 - El nùmero de ciclistas por equipo */
SELECT nomequipo,COUNT(*)total FROM ciclista GROUP BY nomequipo;

/* 7 - El número total de puertos */
SELECT COUNT(*)totalPuerto FROM puerto;

/* 8 - El número total de puertos mayores de 1500 */
SELECT COUNT(*)totalPuerto FROM puerto WHERE altura>1500;

/* 9 - Listar el nombre de los equipos que tengan más de 4 ciclistas  */
SELECT nomequipo,COUNT(*) nciclistas FROM ciclista GROUP BY nomequipo HAVING nciclistas>4;

-- Con subConsulta
SELECT c1.nomequipo FROM (SELECT nomequipo,COUNT(*)total FROM ciclista GROUP BY nomequipo)c1 WHERE c1.total>4;

/* 10 - Listar el nombre de los equipos que tengan más de 4 ciclistas cuya edad este entre 28 y 32 */
SELECT 
  nomequipo,COUNT(*)total 
FROM 
  ciclista 
WHERE 
  edad BETWEEN 28 AND 32 
GROUP BY 
  nomequipo HAVING total>4 ;

/* 11 - Indícame el número de etapas que ha ganado cada uno de los ciclistas */
SELECT 
  dorsal,COUNT(*)totalEtapas 
FROM 
  etapa 
GROUP BY 
  dorsal;

/* 12 - Indícame el dorsal de los ciclistas que hayan ganado más de 1 etapa */
SELECT 
  dorsal 
FROM 
  etapa 
GROUP BY 
  dorsal 
HAVING 
  COUNT(*)>1;


-- la edad maxima de los ciclista
SELECT MAX(edad) FROM ciclista;

-- el ciclista cuta edad es la maxima
SELECT * FROM ciclista WHERE edad=(SELECT MAX(edad) FROM ciclista);

-- el ciclista cuta edad es la minima
SELECT * FROM ciclista WHERE edad=(SELECT MIN(edad) FROM ciclista);